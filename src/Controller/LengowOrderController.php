<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Entity\Order;
use App\Entity\OrderLine;
use App\Service\OrderConsumer;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class LengowOrderController extends AbstractController
{
    /**
     * @Route("/orders/last", name="lengow_orders_last")
     */
    public function ordersLast()
    {
        //
        // Question 1 :
        //
        // - Lister les 20 dernières commandes dont le statut est "new"
        //
        // -> Implémenter la méthode getLastNewOrders()
        // -> Utiliser le queryBuilder, trier par date et id decroissant.
        //

        $orders = $this->getDoctrine()
            ->getRepository(Order::class)
            ->getLastNewOrders();

        return $this->render('lengow_order/new_orders.html.twig', [
            'orders' => $orders,
        ]);
    }

    /**
     * @Route("/orders/last_optimized", name="lengow_orders_last_optimized")
     */
    public function ordersLastOptimized()
    {
        //
        // Question 2 :
        //
        // - Lister les 20 dernières commandes dont le statut est "new", version optimisée
        //
        // -> Implémenter la méthode getLastNewOrdersOptimized()
        // -> Utiliser le queryBuilder, trier par date et id decroissant.
        //
        // -> La page ne doit générer qu'une seule requête SQL
        //

        $orders = $this->getDoctrine()
            ->getRepository(Order::class)
            ->getLastNewOrdersOptimized();

        return $this->render('lengow_order/new_orders.html.twig', [
            'orders' => $orders,
        ]);
    }

    /**
     * @Route("/orders/new", name="lengow_orders_new")
     * @param \Symfony\Component\Routing\RouterInterface $routerInterface
     * @param \Symfony\Component\HttpFoundation\Request  $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function ordersNew(RouterInterface $routerInterface, Request $request)
    {
        //
        // Question 3 :
        //
        // - Consommer l'API /api/orders/new et enregistrer les nouvelles commandes en base
        //
        // -> Effectuer le travail le plus simplement dans le controlleur.
        // -> Afficher le nombre total de commandes dont le statut est "new"
        //

        // Consommation de l'API
        $curl = curl_init();
        $url =  sprintf(
            '%s://%s%s',
            $request->getScheme(),
            $this->getParameter('lengow_test_host'),
            $routerInterface->generate('lengow_api_orders_new')
        );

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        $results = json_decode($result, true);

        curl_close($curl);

        if (!empty($results)) {
            // get all customers in api orders data
            $customers = $this->getDoctrine()->getRepository(Customer::class)
                              ->findBy(['id' => array_column($results, 'customer_id')]);
            try {
                // Enregistrement en BDD
                foreach ($results as $result) {

                    $order = new Order();
                    foreach ($customers as $customer) {
                        if ($customer->getId() === $result['customer_id']) {
                            $order->setCustomer($customer);
                            break;
                        }
                    }

                    if (!$order->getCustomer()) {
                        continue;
                    }
                    $order
                        ->setStatus(Order::STATUS_NEW)
                        ->setCreatedAt(new DateTime($result['date']['date']));

                    foreach ($result['orderlines'] as $orderLine) {
                        $line = new OrderLine();
                        $line
                            ->setPrice($orderLine['price'])
                            ->setProduct($orderLine['product'])
                            ->setQuantity($orderLine['quantity']);

                        $order->addOrderLine($line);
                    }

                    $this->getDoctrine()->getManager()->persist($order);
                }
                $this->getDoctrine()->getManager()->flush();
            } catch (\Exception $e) {
                throw new \Exception(sprintf('Error during saving order %s', $order->getId()));
            }
        }

        // Décompte des commandes dont le statut est "new"
        $totalNewOrders = $this->getDoctrine()
            ->getRepository(Order::class)
            ->countNewOrders();

        return $this->render('lengow_order/get_new_orders.html.twig', [
            'total_new_orders' => $totalNewOrders,
        ]);
    }

    /**
     * @Route("/orders/new_service", name="lengow_orders_new_service")
     * @param \App\Service\OrderConsumer                 $orderConsumer
     * @param \Symfony\Component\HttpFoundation\Request  $request
     * @param \Symfony\Component\Routing\RouterInterface $routerInterface
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\CssSelector\Exception\InternalErrorException
     */
    public function ordersNewService(OrderConsumer $orderConsumer, Request $request, RouterInterface $routerInterface)
    {
        //
        // Question 4 :
        //
        // - Consommer l'API /api/orders/new et /api/orders/new_xml et enregistrer les nouvelles commandes en base
        //   en utilisant les services.
        //
        // -> Le service prendra en paramètre l'url de l'API et devra traiter les commandes quelque soit le format de l'API (JSON et XML).
        // -> L'ajout d'un ou plusieurs autres format d'API ne devra générer aucune modification de la classe de service.
        //

        $url = sprintf(
            '%s://%s%s',
            $request->getScheme(),
            $this->getParameter('lengow_test_host'),
            $routerInterface->generate('lengow_api_orders_new')
        );

        if (!$orderConsumer->createFromUrl($url, new JsonEncoder())) {
            throw new \Exception(sprintf('Error during json api consume'));
        }

        // Consommation de l'API XML
        $url = sprintf(
            '%s://%s%s',
            $request->getScheme(),
            $this->getParameter('lengow_test_host'),
            $routerInterface->generate('lengow_api_orders_new_xml')
        );

        if (!$orderConsumer->createFromUrl($url, new XmlEncoder())) {
            throw new \Exception(sprintf('Error during xml api consume'));
        }

        // Décompte des commandes dont le statut est "new"
        $totalNewOrders = $this->getDoctrine()
                               ->getRepository(Order::class)
                               ->countNewOrders();

        return $this->render('lengow_order/get_new_orders.html.twig', [
            'total_new_orders' => $totalNewOrders,
        ]);
    }

    /**
     * @Route("/orders/jquery", name="lengow_orders_jquery")
     */
    public function orderJqueryLoad()
    {
        //
        // Question 5 :
        //
        // - Consommer l'API /api/orders/random en "Ajax" pour récupérer des commandes.
        //
        // -> Utiliser jQuery pour effectuer la requête Ajax
        // -> Filtrage en javascript des résultats sur la sélection du statut.
        //

        return $this->render('lengow_order/jquery_orders.html.twig');
    }

    /**
     * @Route("/orders/vanilla_js", name="lengow_orders_vanilla_js")
     */
    public function orderVanillaJsLoad()
    {
        //
        // Question 6 :
        //
        // - Consommer l'API /api/orders/random en "Ajax" pour récupérer des commandes.
        //
        // -> Utiliser du pure javascript pour effectuer tous les traitements (chargement, affichage, filtre).
        // -> Doit être fonctionnel sous Chrome
        //

        return $this->render('lengow_order/vanilla_js_orders.html.twig');
    }
}
