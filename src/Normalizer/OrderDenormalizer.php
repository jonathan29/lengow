<?php

namespace App\Normalizer;

use App\Entity\Order;
use InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;
use \Symfony\Component\Serializer\Exception\InvalidArgumentException as SerializerInvalidArgumentException;

/**
 * Class OrderDenormalizer
 *
 * @package App\Normalizer
 */
class OrderDenormalizer implements DenormalizerInterface, SerializerAwareInterface
{
    private $serializer;

    /**
     * @param mixed  $data
     * @param string $class
     * @param null   $format
     * @param array  $context
     *
     * @return \App\Entity\Order|object
     * @throws \Exception
     */
    public function denormalize($data, $class, $format = null, array $context = []): Order
    {
        $order = new Order();

        if (false === ($date = $this->searchDate($data['date']))) {
            throw new SerializerInvalidArgumentException('Unable to get Date');
        }

        $order
            ->setCustomerId($data['customer_id'])
            ->setCreatedAt($date);

        $lines = $this->serializer->denormalize($data['orderlines'], 'App\Entity\OrderLine[]', $format, $context);

        foreach ($lines as $line) {
            $order->addOrderLine($line);
        }

        return $order;
    }

    /**
     * Check if data are supported by denormalizer.
     *
     * @param mixed  $data
     * @param string $type
     * @param null   $format
     * @param array  $context
     *
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null, array $context = [])
    {
        return is_array($data)
               && array_key_exists('date', $data)
               && array_key_exists('customer_id', $data)
               && array_key_exists('orderlines', $data);
    }

    /**
     * {@inheritdoc}
     */
    public function setSerializer(SerializerInterface $serializer)
    {
        if (!$serializer instanceof DenormalizerInterface) {
            throw new InvalidArgumentException('Expected a serializer that also implements DenormalizerInterface.');
        }

        $this->serializer = $serializer;
    }

    /**
     * Search for a valid date.
     *
     * @param array $data
     *
     * @return \DateTimeInterface|false
     */
    private function searchDate($data)
    {
        $data = is_string($data) ? [$data] : $data;

        foreach ($data as $d) {
            if ($newDate = $this->getDate($d)) {
                return $newDate;
            }
        }

        return false;
    }

    /**
     * @param $date
     *
     * @return bool|\DateTime
     */
    private function getDate($date)
    {
        if (is_string($date) && false !== ($time = strtotime($date))) {
            $newDate = new \DateTime();
            $newDate->setTimestamp($time);

            return $newDate;
        }

        return false;
    }
}
