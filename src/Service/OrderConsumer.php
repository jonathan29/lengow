<?php

namespace App\Service;

use App\Entity\Customer;
use App\Entity\Order;
use App\Normalizer\OrderDenormalizer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class OrderConsumer
 *
 * @package App\Service
 */
class OrderConsumer
{
    /** @var \Doctrine\ORM\EntityManagerInterface $em */
    private $em;

    /**
     * OrderConsumer constructor.
     *
     * @param \Doctrine\ORM\EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Save orders in databases.
     *
     * @param string                                                 $url
     * @param \Symfony\Component\Serializer\Encoder\EncoderInterface $encoder
     *
     * @return bool
     */
    public function createFromUrl(string $url, EncoderInterface $encoder): bool
    {
        if (null === $encoder::FORMAT) {
            throw new \InvalidArgumentException('Missing constant format encoder');
        }

        // api query
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);
        try {
            // transform data
            $serializer = new Serializer(
                [new OrderDenormalizer(), new GetSetMethodNormalizer(), new ArrayDenormalizer()],
                [$encoder]
            );

            $orders = $serializer->deserialize($result, 'App\Entity\Order[]', $encoder::FORMAT);

            // saving
            if (!empty($orders)) {
                $customerIds = array_map(function ($o) {
                    return $o->getCustomerId();
                }, $orders);

                // get all customers in api orders data
                $customers = $this->em->getRepository(Customer::class)->findBy(['id' => $customerIds]);


                // Enregistrement en BDD
                /** @var Order $order */
                foreach ($orders as $order) {

                    foreach ($customers as $customer) {
                        if ($customer->getId() === $order->getCustomerId()) {
                            $order->setCustomer($customer);
                            break;
                        }
                    }

                    if (!$order->getCustomer()) {
                        continue;
                    }

                    $order
                        ->setStatus(Order::STATUS_NEW);

                    $this->em->persist($order);

                }
                $this->em->flush();
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}
